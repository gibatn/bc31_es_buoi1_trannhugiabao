let tinhDTB = (...restParam) => {
  let tong = 0;
  for (let i = 0; i < restParam.length; i++) {
    tong = tong + restParam[i];
  }
  return tong / restParam.length;
};
let diemCap1 = () => {
  diemToan = document.getElementById("inpToan").value * 1;
  diemLy = document.getElementById("inpLy").value * 1;
  diemHoa = document.getElementById("inpHoa").value * 1;
  let diemC1 = Math.round(tinhDTB(diemToan, diemLy, diemHoa));

  console.log(diemC1);

  document.getElementById("tbKhoi1").innerHTML = diemC1;
};
let diemCap2 = () => {
  diemVan = document.getElementById("inpVan").value * 1;
  diemSu = document.getElementById("inpSu").value * 1;
  diemDia = document.getElementById("inpDia").value * 1;
  diemEnglish = document.getElementById("inpEnglish").value * 1;
  let diemC2 = tinhDTB(diemVan, diemSu, diemDia, diemEnglish);

  diemC2.toFixed(4);
  document.getElementById("tbKhoi2").innerHTML = diemC2;
};
document.getElementById("btnKhoi1").addEventListener("click", () => diemCap1());
document.getElementById("btnKhoi2").addEventListener("click", () => diemCap2());
